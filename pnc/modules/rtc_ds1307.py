import utime

from ..port import get_port

class RTC_DS1307:
    DS1307_I2C_ADDR = const(0x68)

    port = None

    def __init__(self, port):
        self.port = get_port(port)

    def get_timestamp(self) -> int:
        buf = self.port.mem_read(0x07, DS1307_I2C_ADDR, 0x00)
        print(buf)
        time_tuple = self._register_to_time(buf)
        return utime.mktime(time_tuple)

    def set_timestamp(self, timestamp: int):
        buf = self._time_to_register(timestamp)
        self.port.mem_write(buf, DS1307_I2C_ADDR, 0x00)

    @staticmethod
    def _bcd_to_dec(bcd: int) -> int:
        return (bcd >> 4) * 10 + (bcd & 0b00001111)

    @staticmethod
    def _dec_to_bcd(dec: int) -> int:
        return ((dec // 10) << 4) + (dec % 10)

    @staticmethod
    def _register_to_time(buf: bytes):
        second = RTC_DS1307._bcd_to_dec(buf[0])
        minute = RTC_DS1307._bcd_to_dec(buf[1])

        hour   = None
        if (buf[2] & (1 << 6)) == 0:
            # 24-hour mode
            hour  = RTC_DS1307._bcd_to_dec(buf[2])
        else:
            # 12-hour mode (AM / PM)
            am_pm = buf[2] & (1 << 5)
            hour  = ((buf[2] & (1 << 4)) * 10) + (buf[2] & 0b00001111)

            if (hour == 12):
                hour = 0
            if (am_pm == 1):
                # PM
                hour += 12

        weekday = RTC_DS1307._bcd_to_dec(buf[3])
        day     = RTC_DS1307._bcd_to_dec(buf[4])
        month   = RTC_DS1307._bcd_to_dec(buf[5])

        # 21st century bois
        year  = 2000 + RTC_DS1307._bcd_to_dec(buf[6])

        print((year, month, day, hour, minute, second, weekday, None))

        return (year, month, day, hour, minute, second, weekday, None)

    @staticmethod
    def _time_to_register(timestamp: int):
        time_tuple = utime.localtime(timestamp)

        buf = bytearray(7)
        buf[0] = RTC_DS1307._dec_to_bcd(time_tuple[5])
        buf[1] = RTC_DS1307._dec_to_bcd(time_tuple[4])
        buf[2] = RTC_DS1307._dec_to_bcd(time_tuple[3])
        buf[3] = (time_tuple[6] + 1)
        buf[4] = RTC_DS1307._dec_to_bcd(time_tuple[2])
        buf[5] = RTC_DS1307._dec_to_bcd(time_tuple[1])
        buf[6] = RTC_DS1307._dec_to_bcd(time_tuple[0] - 2000)

        print(buf)

        return buf

