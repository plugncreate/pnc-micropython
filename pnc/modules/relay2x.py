import pyb

from ..port import get_port

class Relay2x:
    port = None
    relays = None

    def __init__(self, port):
        self.port = get_port(port)
        self.relays = [self.port.pin1, self.port.pin2]
        for relay in self.relays:
            relay.init(mode = pyb.Pin.OUT)

    def on(self, num: int):
        num -= 1
        self.relays[num].value(True)

    def off(self, num: int):
        num -= 1
        self.relays[num].value(False)

    def toggle(self, num: int):
        num -= 1
        curr_value = self.relays[num].value()
        self.relays[num].value(not curr_value)

    def status(self, num: int) -> bool:
        return bool(self.relays[num].value())