from .http_request import HTTPRequest
from ...port import get_port, UARTPort

def readline_crlf(uart):
    result = ""

    while not result.endswith("\r\n"):
        tmp = uart.readchar()
        if tmp == -1:
            return result
        result += chr(tmp)

    print("result: {}".format(result))
    return result.strip()


def flush(uart):
    uart.read()


def escape_string(string):
    return string.replace("\\", "\\\\").replace(",", "\\,").replace("\"", "\\\"")


class ESP8266:
    curr_ssid = None
    uart = None

    MODE_STA = const(0x01)
    MODE_AP = const(0x02)
    MODE_STA_AP = const(0x03)

    socket = []

    def __init__(self, port, ssid = None, password = None):
        self.uart = get_port(port)
        if (type(self.uart) is not UARTPort):
            raise ValueError("ESP8266 port must be a UART port")
        self.uart.init(912600)

        self.execute_simple_command("ATE0")
        self.execute_simple_command("AT+UART_DEF=912600,8,1,0,0")
        self.execute_simple_command("AT+CIPMUX=1")

        self.sockets = []
        for i in range(5):
            self.sockets.append(ESP8266HTTPSession(self, i))

        if (ssid is not None) and (password is not None):
            self.connect_to_wifi(ssid, password)

    def execute_simple_command(self, command):
        self.uart.write(command + "\r\n")

        while True:
            result = readline_crlf(self.uart)
            if result == "OK":
                return True
            elif result == "ERROR":
                return False

    def set_wifi_mode(self, mode):
        if mode not in [self.MODE_STA, self.MODE_AP, self.MODE_STA_AP]:
            raise ValueError("mode must be MODE_AP, MODE_STA or MODE_STAT_AP")

        self.execute_simple_command("AT+CWMODR_CUR={}".format(mode))

    def connect_to_wifi(self, ssid, password, bssid = None):
        ssid = escape_string(ssid)
        password = escape_string(password)

        command = 'AT+CWJAP_CUR="{}","{}"\r\n'.format(ssid, password)
        if bssid is not None:
            command += ',"{}"'.format(bssid)

        self.uart.write(command)

        err = ""
        while True:
            line = readline_crlf(self.uart)
            if line == "OK":
                return True
            if line == "FAIL":
                raise WifiConnectionError(err)
            if line.startswith("+CWJAP:"):
                err_code = int(line.replace("+CWJAP:", ""))
                err = ""
                if err_code == 1:
                    err = "connection timeout"
                elif err_code == 2:
                    err = "wrong password"
                elif err_code == 3:
                    err = "AP not found"
                elif err_code == 4:
                    err = "connection failed"

    def disconnect_from_wifi(self):
        self.execute_simple_command("AT+CWQAP")

    def list_available_ap(self):
        self.uart.write("AT+CWLAP\r\n")

        result = []
        while True:
            line = readline_crlf(self.uart)
            if line == "OK":
                break
            result.append(line.split(","))

        return result

    def get(self, *args, **kwargs):
        for sock in self.sockets:
            if not sock.connected:
                return sock.get(*args, **kwargs)
        raise ESP8266SocketExhausted

    def post(self, path, **kwargs):
        pass


class ESP8266HTTPSession:
    esp8266 = None
    link_id = None
    host = None
    port = None
    connected = None

    def __init__(self, esp8266, link_id):
        self.esp8266 = esp8266
        self.link_id = link_id
        self.connected = False

    def get(self, host, port, path, headers = None, body = None):
        if not (self.connect(host, port)):
            self.close()
            raise ESP8266ConnectionError

        request = HTTPRequest("GET", host, path, headers, body)

        if not (self.send(request.build_request())):
            self.close()
            raise ESP8266SendError

        self.close()
        # raw_response = self.read_response(timeout)
        # if raw_response is None:
        #     raise ESP8266TimeoutError
        # response = HTTPResponse.parse_response(raw_response)
        # return response

    # def read_response(self, timeout):

    def send(self, request_body):
        print(request_body)
        length = len(request_body)
        if not self.esp8266.execute_simple_command("AT+CIPSEND={},{}".format(self.link_id, length)):
            return False

        while True:
            line = readline_crlf(self.esp8266.uart)
            if line == "> ":
                break
        self.esp8266.uart.write(request_body)

        while True:
            line = readline_crlf(self.esp8266.uart)
            if line == "ERROR":
                return False
            if line == "SEND OK":
                return True

    def post(self, host, port, path, data):
        pass

    def connect(self, host, port):
        self.host = host
        self.port = port

        command = 'AT+CIPSTART={},"TCP","{}",{}'.format(self.link_id, self.host, self.port)

        self.connected = self.esp8266.execute_simple_command(command)

        return self.connected

    def close(self):
        flush(self.esp8266.uart)
        self.esp8266.execute_simple_command("AT+CIPCLOSE={}".format(self.link_id))
        self.connected = False

    pass


class WifiConnectionError(Exception):
    pass


class ESP8266ConnectionError(Exception):
    pass


class ESP8266SendError(Exception):
    pass


class ESP8266TimeoutError(Exception):
    pass


class ESP8266SocketExhausted(Exception):
    pass
