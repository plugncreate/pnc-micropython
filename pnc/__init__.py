import pyb
import utime

from .modules.dht11 import DHT11
from .modules.dht22 import DHT22
from .modules.esp8266 import ESP8266, ThingSpeakChannel
from .modules.lcd import LCD_16x2
from .modules.numpad import Numpad_4x4
from .modules.servo import Servo
from .modules.relay2x import Relay2x
from .modules.ultrasonic import Ultrasonic
from .modules.rtc_ds1307 import RTC_DS1307

# set CPU frequency to 144Mhz so that some prepherial works (like DHT11)
pyb.freq(144000000)
